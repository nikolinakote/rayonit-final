var stompClient = null;

function connect() {
    var socket = new SockJS('/get-msg-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/messages', function (message) {
            console.log("here");
            showMessage(JSON.parse(message.body).content);
        });
    });
}
function showMessage(message) {

    $("#messages").append("<tr><td>" + message + "</td></tr>");
}
$(function(){
   connect();
});