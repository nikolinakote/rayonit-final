package com.example.Rayonittest;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Messages")
public class Message {

    private String content;

    public Message(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}
