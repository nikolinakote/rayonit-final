package com.example.Rayonittest;

import com.example.Rayonittest.config.MessageRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageDb implements CommandLineRunner {
    private MessageRepo messageRepo;

    public MessageDb(MessageRepo messageRepo) {
        this.messageRepo = messageRepo;
    }


    public void insert(Message message) {
        messageRepo.save(message);
    }

    public List<Message> getMessages() {
        return messageRepo.findAll();
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
