package com.example.Rayonittest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class RayonitTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(RayonitTestApplication.class, args);
    }

}
