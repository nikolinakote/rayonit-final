package com.example.Rayonittest.config;

import com.example.Rayonittest.Message;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MessageRepo extends MongoRepository<Message,String> {
}
