package com.example.Rayonittest;


import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import io.reactivex.subscribers.ResourceSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.HtmlUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Controller
public class MessageController {
    private static final Logger LOG = LoggerFactory.getLogger("RayonitTestApplication");
    private SimpMessagingTemplate template;
    private MessageDb messageDb;
    private Subject subject;

    @Autowired
    public MessageController(SimpMessagingTemplate template, MessageDb messageDb) {
        this.template = template;
        this.messageDb = messageDb;
        initSubject();

    }

    @RequestMapping("/")
    public String index() {
        getMessagesFromDb()
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe();

        return "index.html";
    }

    private Observable<Message> getMessagesFromDb() {
        return Observable
                .just(messageDb)
                .delay(500, TimeUnit.MILLISECONDS)
                .map(messageDb -> messageDb.getMessages())
                .flatMap((Function<List<Message>, ObservableSource<Message>>) messages -> Observable.fromIterable(messages))
                .map(message -> {
                    subject.onNext(message);
                    return message;
                })
                ;
    }

    private void initSubject() {
        subject = PublishSubject
                .create();
        subject
                .toFlowable(BackpressureStrategy.BUFFER)
                .subscribe(new ResourceSubscriber() {
                    @Override
                    public void onNext(Object o) {
                        if (o instanceof String) {
                            messageDb.insert(new Message((String) o));
                            sendMessage((String) o);
                        } else {
                            LOG.info("Received message: {}", (((Message) o).getContent()));
                            sendMessage(((Message) o).getContent());
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void sendMessage(String message) {
        Message myMessage = new Message(HtmlUtils.htmlEscape(message));
        MessageController.this.template.convertAndSend("/topic/messages", myMessage);
    }

    @KafkaListener(topics = "test", groupId = "first-group")
    public void listen(String message) {

        LOG.info("Received message: {}", message);
        subject.onNext(message);

    }


}
